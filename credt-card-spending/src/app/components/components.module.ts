import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';

import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';

import { TabsComponent } from './tabs/tabs.component';
import { HeaderComponent } from './header/header.component';
import { ListComponent } from './list/list.component';

@NgModule({
  declarations: [TabsComponent, HeaderComponent, ListComponent],
  imports: [CommonModule, MatTabsModule, MatListModule],
  exports: [TabsComponent, HeaderComponent, ListComponent],
  providers: [CurrencyPipe],
})
export class ComponentsModule {}
