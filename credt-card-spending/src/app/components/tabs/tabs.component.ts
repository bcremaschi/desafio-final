import { Lists } from './../../pages/interfaces/lists';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnInit {
  @Input() historyItems: Array<Lists>;
  @Input() monthItems: Array<Lists>;
  @Input() categoryItems: Array<Lists>;

  constructor() {}

  ngOnInit() {}
}
