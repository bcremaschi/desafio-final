import { Component, OnInit } from '@angular/core';

import { InvoiceHistoryService } from './../services/invoice-history.service';
import { CategoriesService } from './../services/categories.service';
import { Lists } from './../interfaces/lists';
import { Invoices } from '../interfaces/invoices';

const MONTH_NAMES = [
  'Janeiro',
  'Fevereiro',
  'Março',
  'Abril',
  'Maior',
  'Junho',
  'Julho',
  'Agosto',
  'Setembro',
  'Outubro',
  'Novembro',
  'Dezembro',
];

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public historyItems: Array<Lists> = [];
  public monthItems: Array<Lists> = [];
  public categoryItems: Array<Lists> = [];

  constructor(
    private invoiceHistoryService: InvoiceHistoryService,
    private categoriesService: CategoriesService
  ) {}

  ngOnInit() {
    this.getInvoiceHistory();
  }

  private getInvoiceHistory(): void {
    this.invoiceHistoryService.get().subscribe(
      (response: Array<Invoices>) => {
        this.setHistoryItems(response);
        this.setMonthItems(response);
        this.setCategoryItems(response);
      },
      (error) => console.log(error)
    );
  }

  async getCateroryName(category: string) {
    let categoryName: string;
    await this.categoriesService.get(category).then((res) => {
      categoryName = res.nome;
    });
    return categoryName;
  }

  private setHistoryItems(invoices: Array<Invoices>): void {
    invoices.map(async (invoice) => {
      this.historyItems.push({
        value: invoice.valor,
        conlidatedType: invoice.origem,
      });
    });
  }

  private setMonthItems(invoices: Array<Invoices>): void {
    invoices.map(async (invoice) => {
      const month = MONTH_NAMES[invoice.mes_lancamento];
      this.monthItems = this.handleConsolidateditems(
        invoice,
        month,
        this.monthItems
      );
    });
  }

  private setCategoryItems(invoices: Array<Invoices>): void {
    invoices.map(async (invoice) => {
      const contegory = await this.getCateroryName(invoice.categoria);
      this.categoryItems = this.handleConsolidateditems(
        invoice,
        contegory,
        this.categoryItems
      );
    });
  }

  private handleConsolidateditems(
    invoice: Invoices,
    value: string,
    consolidateditems: Array<Lists>
  ): Array<Lists> {
    const foundCategory = consolidateditems.findIndex(
      (item) => item.conlidatedType === value
    );

    if (foundCategory > 0 && consolidateditems.length) {
      consolidateditems[foundCategory].value += invoice.valor;
    } else {
      consolidateditems.push({
        value: invoice.valor,
        conlidatedType: value,
      });
    }

    return consolidateditems;
  }
}
