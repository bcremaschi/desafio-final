import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { HomeComponent } from './home/home.component';

import { MatTabsModule } from '@angular/material/tabs';

import { CoreModule } from './../core/core.module';
import { ComponentsModule } from './../components/components.module';

@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, ComponentsModule, CoreModule, MatTabsModule],
  exports: [HomeComponent],
})
export class PagesModule {}
