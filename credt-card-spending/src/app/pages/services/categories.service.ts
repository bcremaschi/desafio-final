import { Injectable } from '@angular/core';

import { HttpService } from 'src/app/core/services/http.service';
import { Categories } from '../interfaces/categories';

@Injectable({
  providedIn: 'root',
})
export class CategoriesService {
  constructor(private httpService: HttpService) {}

  get(category: string): Promise<Categories> {
    return this.httpService.getPromise('/categorias/' + category);
  }
}
