import { TestBed } from '@angular/core/testing';

import { InvoiceHistoryService } from './invoice-history.service';

describe('InvoiceHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InvoiceHistoryService = TestBed.get(InvoiceHistoryService);
    expect(service).toBeTruthy();
  });
});
