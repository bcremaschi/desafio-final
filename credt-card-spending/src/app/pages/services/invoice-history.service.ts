import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpService } from 'src/app/core/services/http.service';
import { Invoices } from '../interfaces/invoices';

@Injectable({
  providedIn: 'root',
})
export class InvoiceHistoryService {
  constructor(private httpService: HttpService) {}

  get(): Observable<Array<Invoices>> {
    return this.httpService.get('/lancamentos');
  }
}
